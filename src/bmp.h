#ifndef BMP_H
#define BMP_H

#include <string>
#include <vector>
#include <cstdint>

void write_bmp(std::string filename, std::vector<uint8_t> pixels);

#endif
