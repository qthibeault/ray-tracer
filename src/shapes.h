#ifndef SHAPES_H
#define SHAPES_H

class Shape
{
};

class Sphere : public Shape
{
	private:
		unsigned int radius;
	
	public:
		Sphere(unsigned int radius): radius{radius} {}
};

#endif
