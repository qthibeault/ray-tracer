#ifndef SCENE_H
#define SCENE_H

#include <vector>

#include "shapes.h"

class Scene
{
	private:
		std::vector<Shape> shapes;

	public:
		Scene(std::vector<Shape> shapes): shapes{shapes} {}
};

#endif
