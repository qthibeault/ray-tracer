#ifndef RENDERER_H
#define RENDERER_H

#include <vector>
#include <cstdint>

#include "scene.h"

class Renderer
{
	private:
		unsigned int height, width;

	public:
		Renderer(unsigned int height, unsigned int width)
			: height{height}, width{width} {}

		std::vector<uint8_t> render(Scene s);
};

#endif
