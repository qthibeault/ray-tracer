#include <vector>
#include <fstream>
#include <cstdlib>
#include <cstdint>
#include <iostream>

#include "shapes.h"
#include "scene.h"
#include "renderer.h"
#include "bmp.h"

int main(int argc, char *argv[])
{
	if (argc != 2) {
		std::cerr << "Usage: " << argv[0] << " <image name>" << std::endl;
		return EXIT_FAILURE;
	}

	std::vector<Shape> shapes{Sphere(10), Sphere(20)};
	Scene spheres(shapes);

	Renderer renderer(240, 480);
	std::vector<uint8_t> image = renderer.render(spheres);

	std::string filename = std::string(argv[1]) + ".bmp";
	write_bmp(filename, image);

	return EXIT_SUCCESS;
}
